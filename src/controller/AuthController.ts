import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IAuthController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";
import { IUser } from "../domain/interfaces/IUser.interface";
import { IAuth } from "../domain/interfaces/IAuth.interface";
//ORM -- Colecciones De la carpeta USERS
import { registerUser, loginUser, logoutUser, getUserById } from "../domain/orm/User.orm";
import { AuthResponse, ErrorResponse } from "./types";


@Route ("/api/auth")
@Tags("AuthControllers")

export class AuthController implements IAuthController{
    @Post("/register")
    public async  registerUser(user: IUser): Promise<any> {
        let response: any='';
        if(user){
            LogSuccess(`[api/auth/register] Usuario registrado: ${user.email}`);
         await registerUser(user).then((r)=>{
            LogSuccess(`[api/auth/register] Usuario Creado: ${user.email}`);
            response ={
                message:`User registrado: ${user.name}`
            }
        });
        } else {
            LogWarning('[api/auth/register] Se necesita una entidad de usuario')
            response = {
                message:'Usuario no registrado: Por favor indique una entidad de usuario par crear'
            }
        }
        return response;
    }


    @Post ("/login")
    public async  loginUser(auth: IAuth): Promise<any> {
        let response: AuthResponse | ErrorResponse | undefined;
        if(auth){
          LogSuccess(`[api/auth/login] Usuario logeado: ${auth.email}`);
             let data= await loginUser(auth);
               response ={
                    token: data.token,
                   message:`Userusario logueado: ${data.user.name}`,
                
                      }
                } else{
                        LogWarning('[api/auth/login] Error inicio de sesion(email&password')
                         response = {
                         error:'[Autentication ERROR]: Email y Password son requeridos ',
                         message:'Por favor ingresar un usuario y contraseña valido'
                         }
                    }
                   return response;
    }

      /**
     * Endpoint para obtener la lista en la coleccion "USERS" de la base de datos.
     * Middleware: Valida JWT
     * En la cabecera se debe añadir el x-access-token win un token JWT
     * 
     * @param {string} id ID para buscar (opcional). 
     * @returns Todos los usuarios o algun usuario particular al ingresar ID.
     */
       @Get("/me")
       public async userData(@Query()id?: string): Promise<any> {
           let response: any='';
           if(id){
               LogSuccess(`[api/users] Get User DATA by ID: ${id}`);
            response=await getUserById(id);
            } 
       return response;
       }


        @Post ("/logout")
    public async  logOutUser(auth: any): Promise<any> {
        let response: any='';
        //TODO: Desloguear Usuario
        throw new Error("Method not implemented.");
    }
    
}