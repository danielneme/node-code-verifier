import { Get, Query, Route, Tags } from "@tsoa/runtime";
import { BasicResponse} from "./types";
import {IHelloController}  from "./interfaces";
import { LogSuccess } from "../utils/logger";

@Route("/api/hello")
@Tags("HelloController")
export class HelloController implements IHelloController{
  /**
   * Endpoit para mostrar "Hello {name}" en JSON
   * @param { string | undefined } name Nombre de usuario a saludar
   * @returns { BasicResponse }Promesa de un BasicResponse
   */
  
  @Get("/")
    public async getMessage(@Query() name?: string): Promise<BasicResponse> {
        
       LogSuccess('[/api/hello] Get Request')

       return{
           message: `Hello, ${name || "World!"}`
       }
    }

}

