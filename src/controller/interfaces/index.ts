import { IKata } from "../../domain/interfaces/IKata.interface";
import { IUser } from "src/domain/interfaces/IUser.interface";
import { BasicResponse } from "../types";



export interface IHelloController {
    getMessage(name?:string): Promise<BasicResponse>
}

export interface IUserController{
    //Leer todos los usuarios de la base de datos. || O leer por ID
    getUsers(page: number, limit:number, id?:string): Promise<any>
    //Obtener las katas del usuario
    getKatas( page: number, limit:number,id: string): Promise<any>
    //Borrar los usuarios de la base de datos. 
    deleteUser(id?:string): Promise<any>
   //Actualizar un usuario por ID
   updateUserByID(id:string, user:any): Promise<any>

}

export interface IAuthController{
    //Registrar Usuarios
    registerUser(user:IUser): Promise<any>
    //Login de Usuario
    loginUser(auth:any): Promise<any>

}

export interface IKataController{
    //Leer todos las Katas de la base de datos. || O leer por ID
    getKatas(page: number, limit:number, id?:string): Promise<any>
    //Crear nueva Kata
    createKata(kata:IKata): Promise<any>
    //Borrar las katas de la base de datos. 
    deleteKata(id?:string): Promise<any>
   //Actualizar una kata
   updateKata(id:string, user:IKata): Promise<any>
}