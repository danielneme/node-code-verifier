/**Respuesta Basica de JSON para Controladores */

export type BasicResponse = {
    message: string
}

/**Respuesta de Error de JSON para Controladores */
export type ErrorResponse = {
    error: string,
    message: string
}

/**Autenticacion de JSON para Controladores */
export type AuthResponse = {
     message: string,
     token: string
}