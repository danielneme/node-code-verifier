import { Delete, Get, Post, Put, Query, Route, Tags } from "tsoa";
import { IUserController } from "./interfaces";
import { LogSuccess, LogError, LogWarning } from "../utils/logger";
//ORM -- Colecciones De la carpeta USERS
import { deleteUserById, getAllUsers, getKatasFromUser, getUserById, updateUserByID } from "../domain/orm/User.orm";



@Route ("/api/users")
@Tags("UserControllers")
export class UserController implements IUserController{
   
          
    /**
     * Endpoint para obtener la lista en la coleccion "USERS" de la base de datos.
     * @param {string} id ID para buscar (opcional). 
     * @returns Todos los usuarios o algun usuario particular al ingresar ID.
     */
    @Get("/")
    public async getUsers(@Query() page: number,@Query() limit:number,@Query()id?: string): Promise<any> {
        let response: any='';
        if(id){
            LogSuccess(`[api/users] Get User by ID: ${id}`);
         response=await getUserById(id);

        } else {
        LogSuccess('[/api/users] Get All Users Request')
        response = await getAllUsers(page, limit);
             
         }
    return response;
    }
        /**
     * Endpoint para borrar en la coleccion "USERS" de la base de datos.
     * @param {string} id ID para borrar (opcional). 
     * @returns algun usuario particular al borrar ID.
     */
         @Delete("/")
         public async deleteUser(@Query()id?: string): Promise<any> {
             let response: any='';
             if(id){
                 LogSuccess(`[api/users] Get User by ID: ${id}`);
                 await deleteUserById(id).then((r)=>{
                     response={
                         message:`El usuario con id ${id} se ha borrado satisfactoriamente`
                     }
                 })
                } else {
             LogWarning('[/api/users] Delete Users Request WITHOUT ID')
             response = {
                 message:'Por favor indique un ID para borrar de la Base de datos'
             }
             
              }
         return response;
         }
     
        /**
         * Actualizar usuario por ID
         * @param {string} id Es el id para actualizar 
         * @param {any} user Son los parametros a actualizar
         * @returns 
         */
        @Put("/")
        public async updateUserByID(@Query()id: string, user: any): Promise<any> {
            let response: any='';
            if(id){
                LogSuccess(`[api/users] Update User by ID: ${id}`);
                await updateUserByID(id,user).then((r)=>{
                    response={
                        message:`El usuario con id ${id} se ha actualizado satisfactoriamente`
                    }
                })
               } else {
            LogSuccess('[/api/users] Update Users Request WITHOUT ID')
            response = {
                message:'Por favor indique un ID para actualizar de la Base de datos'
            }
            
             }
        return response;
        }
        @Get('/katas') 
        public async getKatas(@Query() page: number,@Query() limit:number,@Query()id: string): Promise<any> {
            let response: any='';
            if(id){
                LogSuccess(`[api/users/katas] Get Katas from user by ID: ${id}`);
                response=await getKatasFromUser(page, limit,id)
    
            } else {
            LogSuccess('[api/users/katas] Getall katas without ID')
            response = {
                message:'ID from user is Needed'
            }
                 
             }
        return response;
             
    
                 
             }
 }

