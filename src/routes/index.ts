/**
 * Root Router
 * Encargado de gestionar las direcciones.
 */

import express, {Request, Response}  from "express";
import helloRouter from "./HelloRouter";
import { LogInfo } from "../utils/logger";
import usersRouter from "./UserRouter";
import authRouter from "./AuthRouter";
import katasRouter from "./KataRouter";

// Instancia de Servidor
let server = express();
// Instancia de Enrutado
let rootRouter = express.Router();
// Activar peticiones a http://localhost:8080/api
rootRouter.get('/', (req: Request, res: Response) =>{
    LogInfo ('Get: http://localhost:8080/api')
    // Imprimir pantalla
    res.send('Bienvenidos a mi api Restull, la primera de todas! no entiendo nada xD, haciendola con Express + TS + Nodemon + Jest + Swagger + Mongoose');
});
// Redirecciones a Routers
server.use('/', rootRouter); //http://localhost:8080/api
server.use('/hello', helloRouter); //http://localhost:8080/api/hello --> HelloRouter
// Añadir mas routas a la APP
server.use('/users', usersRouter ); //http://localhost:8080/api/users --> UserRouter
// Rutas de autenticacion
server.use('/auth', authRouter);//http://localhost:8080/api/auths --> AuthRouter
//Ruta de Katas
server.use('/katas',katasRouter); //http://localhost:8080/api/katas --> KatasRouter
export default server;