import { BasicResponse } from "../controller/types";
import express, {Request, Response} from "express";
import { HelloController } from "../controller/HelloController";
import { LogInfo } from "../utils/logger";
// Router de Express
let helloRouter = express.Router();

helloRouter.route('/')
//GET =>http://localhost:8080/api/hello?name=Martin
.get (async(req: Request, res: Response)=>{
// Obtener parametro QUERY
let name: any = req?.query?.name;
LogInfo(`Query Param: ${name}`);
// Instancia del controlador para ejecutar el comando method
const controller: HelloController= new HelloController();
//Obtener respuesta
const response: BasicResponse = await controller.getMessage(name);
//Enviar al cliente la respuesta
return res.send(response);

})

//Exportamos el Hello Router
export default helloRouter;
