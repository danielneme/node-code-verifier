import express, {Request, Response} from "express";
import { AuthController } from "../controller/AuthController";
import { IUser } from "../domain/interfaces/IUser.interface";
import { IAuth } from "../domain/interfaces/IAuth.interface";
//BCRYPT for passwords
import bcrypt from 'bcrypt'; 

// Router de Express
let authRouter = express.Router();

//JWT Verifier Middleware
import {verifyToken} from '../middlewares/verifyToken.middleware';

//Bodr Parser
import bodyParser from 'body-parser';
//Middleware para leer el JSON
let jsonParser=bodyParser.json();

authRouter.route('/register')
.post (jsonParser, async(req:Request, res: Response)=>{
     let{name, email, password, age}= req?.body;
     let hashedPassword ='';
     if(name && password && email && age ){
          //Obtener la contraseña en el request y cypher
      hashedPassword = bcrypt.hashSync(password,8);

      let newUser: IUser={
          name: name,
          email: email,
          password: hashedPassword,
          age: age,
          katas: []
      }
// Instancia del controlador para ejecutar el comando method
const controller: AuthController= new AuthController();

      //Obtener respuesta
const response: any = await controller.registerUser(newUser);  
//Enviar al cliente la respuesta
return res.status(200).send(response);    
}
})

authRouter.route('/login')
.post (jsonParser, async(req:Request, res: Response)=>{
     let{email, password}= req?.body;
     let hashedPassword ='';
     if(password && email){
  
// Instancia del controlador para ejecutar el comando method
const controller: AuthController= new AuthController();

let auth: IAuth={
    email:email,
    password:password
}


      //Obtener respuesta
const response: any = await controller.loginUser(auth);  
//Enviar al cliente la respuesta con el response que es el Token
return res.status(200).send(response);    
}else{
    return res.status(400).send({
        message:'[ERROR User data Missing]: No se puede registrar el usuario'
    }); 
}

});

// Ruta protegida por la verificacion del token a travez del middleware creado
authRouter.route('/me')
    .get(verifyToken, async (req: Request, res: Response)=>{
        //Obtener el ID del usuario para chekear su data
        let id: any=req?.query?.id;
        if(id){
            // Controller: Auth Controller
            const controller: AuthController= new AuthController();
            //Obtener la respuesta desde Controller
            let response: any=await controller.userData(id);
            //Si el usuario esta autorizado
            return res.status(200).send(response);
        }else{
            return res.status(401).send({
                message: 'Ustes no esta autorizado consulte a su administrador'
            })
        }


    })

export default authRouter;