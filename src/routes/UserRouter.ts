import express, {Request, Response} from "express";
import { UserController } from "../controller/UsersController";
import { LogInfo } from "../utils/logger";
import helloRouter from "./HelloRouter";
//Body Parser: para leer las peticiones desde el body en vez de desde la query(params)
import bodyParser from "body-parser";
let jsonParser = bodyParser.json;
//JWT Verifier Middleware
import {verifyToken} from '../middlewares/verifyToken.middleware'; 

// Router de Express
let usersRouter = express.Router();


usersRouter.route('/')
//GET =>http://localhost:8080/api/users?id=631bdc56f02b605239ecf15c
.get (verifyToken, async(req: Request, res: Response)=>{
 // Obtener parametro QUERY
let id: any = req?.query?.id;
//Paginacion
let page: any=req?.query?.page||1;
let limit: any=req?.query?.limit||10;
LogInfo(`Query Param: ${id}`);
// Instancia del controlador para ejecutar el comando method
const controller: UserController= new UserController();

//Obtener respuesta
const response: any = await controller.getUsers(page, limit, id)
//Enviar al cliente la respuesta
return res.status(200).send(response);

}) 
//Borrar desde Mongo =>http://localhost:8080/api/users?id=631bdc56f02b605239ecf15c
.delete(verifyToken, async (req:Request, res:Response) => {
     // Obtener parametro QUERY
let id: any = req?.query?.id;
LogInfo(`Query Param: ${id}`);
// Instancia del controlador para ejecutar el comando method
const controller: UserController= new UserController();
//Obtener respuesta
const response: any = await controller.deleteUser(id)
//Enviar al cliente la respuesta
return res.status(204).send(response);
})

//Put:
.put(verifyToken, async (req:Request, res:Response) => {
     let id: any = req?.query?.id;
     let name: any = req?.query?.name;
     let email: any = req?.query?.email;
     let age: any = req?.query?.age;
     LogInfo(`Query Param: ${id},${name}, ${email}, ${age}`);
// Instancia del controlador para ejecutar el comando method
const controller: UserController= new UserController();
let user={
     name: name,
     email: email,
     age: age

}
//Obtener respuesta
const response: any = await controller.updateUserByID(id, user)
//Enviar al cliente la respuesta
return res.status(204).send(response);
});


usersRouter.route('/katas')
//GET =>http://localhost:8080/api/users?id=631bdc56f02b605239ecf15c
.get (verifyToken, async(req: Request, res: Response)=>{
 // Obtener parametro QUERY
let id: any = req?.query?.id;
//Paginacion
let page: any=req?.query?.page||1;
let limit: any=req?.query?.limit||10;
// Instancia del controlador para ejecutar el comando method
const controller: UserController= new UserController();

//Obtener respuesta
const response: any = await controller.getKatas(page, limit, id)
//Enviar al cliente la respuesta
return res.status(200).send(response);

}) 



//Exportamos el User Router
export default usersRouter;


/**
 * 
 * 
 * POST Creacion de documentos ==> 201 OK
 * GET Obtener documentos ==> 200 OK
 * DELETE Borrar Documentos ==> 200 (OK) || 204 (No hay retorno de datos)
 * PUT Actualizar Documentos ==> 200 (OK) || 204 (No hay retorno de datos)
 * 
 * 
 */