import express, {Express, Request, Response} from "express";

//Swagger
import swaggerUi from 'swagger-ui-express';


//Variables de Seguridad
import cors from 'cors';
import helmet from "helmet";
//TODO Gestionar HTTPS
//Root Router
import rootRouter from '../routes';
import mongoose from "mongoose";

// Create Express APP
const server: Express = express();

//Configuracion de ruta para Swagger
server.use(
    '/docs',
    swaggerUi.serve,
    swaggerUi.setup(undefined, {
        swaggerOptions: {
            url: "/swagger.json",
            explorer:true
        }
    })

);

 
//Definir SERVER para utilizar "/api" y que ejecute el rootRouter desde 'indes.ts' en routes
//A partir de este momento lo que tendremos sera http://localhost:8080/api/...
server.use(
    '/api',
    rootRouter
    )

    //Servidor Estatico
    server.use(express.static('public'));

// TODO Mongoose Connection
mongoose.connect('mongodb://127.0.0.1:27017/codeverification');


//Especificar la configuracion de seguridad
server.use(helmet());
server.use(cors());

// Tipo decontenido a controlar
server.use(express.urlencoded({extended: true, limit:'50mb'}));
server.use(express.json({limit:'50mb'}));
//Redirecciones:
//http://localhost:8080 -->http://localhost:8080/api
server.get('/', (req: Request, res: Response) =>{
    res.redirect('/api');
});

export default server;


