import { userEntity } from "../entities/User.entity";
import { LogSuccess, LogError } from "../../utils/logger";
import { IUser } from "../interfaces/IUser.interface";
import { IAuth } from "../interfaces/IAuth.interface";
//Variables de entorno
import dotenv from 'dotenv';


//BCRYPT para gestionar las contraseñas
import bcrypt from 'bcrypt';
//Importar JWT para comprar el token
import jwt from 'jsonwebtoken';
import { UserResponse } from "../types/UserResponse.type";
import { kataEntity } from "../entities/Kata.entity";
import { IKata } from "../interfaces/IKata.interface";

//Configuracion de las variables de entorno
dotenv.config();

//Obtener la Secretkey para generar el JWT
const secret=process.env.SECRETKEY || 'MYSECRETKEY';

//CRUD
/**
 * Metodo para obtener todos los usuarios de la coleccion "users" en Mongo Server.
 */
export const getAllUsers = async(page:number, limit:number): Promise<any[] | undefined> => {
    try{ 
        let userModel =  userEntity();
        let response: any={};
        //Buscar todos los usuarios usando paginacion.
        
        await userModel.find({isDeleted:false})
            //Seleccionamos los campos a mostrar.
            .select('name email age katas')
            //Indicamos la cantidad de campos por pagina.
            .limit(limit)
            //Verificamos cuantas paginas necesitamos mostrar dependiendo de la cantidad de usuarios por limite.
            .skip((page-1)*limit)
            .exec().then((users:IUser[])=>{
                         
                response.users=users;
            });

        //Contar el total de documentos en la coleccion "Users"
        await userModel.countDocuments().then((total:number)=>{
            response.totalPages=Math.ceil(total/limit);
            response.currentePage=page;
        });
        return response;

    } catch (error){
            LogError(`[ORM ERROR]: Getting All Users: ${error}`);
    }
}
// - Get user by ID
export const getUserById = async (id:string) : Promise<any | undefined> =>{
    try{
        let userModel= userEntity();
// Search User By ID
    return await userModel.findById(id).select('name email age katas');
    } catch (error){
        LogError(`[ORM ERROR]: Getting User by ID: ${error}`);
    }
    
}
// - Delet User by ID
export const deleteUserById = async (id:string) : Promise<any | undefined> =>{
    try{
        let userModel= userEntity();
        //Borrando un usuario por ID
        return await userModel.deleteOne({_id : id})
    }catch (error){
        LogError(`[ORM ERROR]: Deleting User by ID: ${error}`);
    }
}

// - Update user by ID
export const updateUserByID= async (id:string, user:any) : Promise<any | undefined> =>{
    try{
        let userModel= userEntity(); 
        //Actaulizar Usuario en MongoDB 
        return await userModel.findByIdAndUpdate(id,user);
    }catch (error){
        LogError(`[ORM ERROR]: Creating User:${id} ${error}`);
    }

}
//Registrar Usuario

export const registerUser= async (user:IUser) : Promise<any | undefined> =>{
try{
    let userModel= userEntity(); 
    //Creado de Usuario en MongoDB 
    return await userModel.create(user);
}catch (error){
    LogError(`[ORM ERROR]: Registrando User: ${error}`);
}
}

//Logear Usuario
export const loginUser= async (auth:IAuth) : Promise<any | undefined> =>{
    try{
        let userModel= userEntity(); 
        let userFound: IUser | undefined = undefined;
        let token = undefined;

        //Vericia si el usuario existe por Email
        await userModel.findOne({email:auth.email}).then((user:IUser)=>{
            userFound=user;
        }).catch((error)=>{
            console.error('[ERROR de Autenticacion en ORM]: Usuario no encontrado');
            throw new Error(`[ERROR de Autenticacion en ORM]: Usuario no encontrado: ${error}`);
        });

        //Verifica si el Password es valido (comparandolo con bcrypt)
        let validPassword=bcrypt.compareSync(auth.password,userFound!.password);
        if (!validPassword){
            console.error('[ERROR de Autenticacion en ORM]:Contraseña no valida');
            throw new Error(`[ERROR de Autenticacion en ORM]: Contraseña no valida:`);
        }
        //Generamos nuestro JWT
        token=jwt.sign({email: userFound!.email},secret,{
            expiresIn:"3h"
        });
        return{
            user:userFound,
            token: token
        }
 
       
     } catch (error){
        LogError(`[ORM ERROR]: Registrando Usuario: ${error}`);
    }

}
//Deslogear Usuario
//TODO: No implementado
export const logoutUser= async ( ) : Promise<any | undefined> =>{

}

//CRUD
/**
 * Metodo para obtener todas las Katas de la coleccion "users" en Mongo Server.
 */
 export const getKatasFromUser = async(page:number, limit:number, id:string): Promise<any[] | undefined> => {
    try{ 
        let userModel =  userEntity();
        let katasModel=kataEntity();
        let katasFound: IKata[]=[];
        let response: any={katas:[]};
        //Buscar todas las katas usando paginacion.
        
        await userModel.findById(id).then(async(user:IUser)=>{
            
            response.user=user.email;

           await katasModel.find({"_id":{"$in": user.katas}}).then((katas:IKata[])=>{
                katasFound=katas;
                
        
            });
        }).catch((error)=>{
            LogError(`[ORM ERROR]: Obteniendo Katas de usuario:${error}`);
        })
        response.katas=katasFound;
         return response;

    } catch (error){
            LogError(`[ORM ERROR]: Getting Katas by User: ${error}`);
    }
}