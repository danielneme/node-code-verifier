import { kataEntity } from "../entities/Kata.entity";
import { LogSuccess, LogError } from "../../utils/logger";
import { IKata } from "../interfaces/IKata.interface";

//Variables de entorno
import dotenv from 'dotenv';

//Configuracion de las variables de entorno
dotenv.config();


//CRUD
/**
 * Metodo para obtener todos las Katas de la coleccion "Kata" en Mongo Server.
 */
export const getAllKatas = async(page:number, limit:number): Promise<any[] | undefined> => {
    try{ 
        let kataModel =  kataEntity();
        let response: any={};
        //Buscar todas las katas usando paginacion.
        
        await kataModel.find({isDeleted:false})
            //Indicamos la cantidad de campos por pagina.
            .limit(limit)
            //Verificamos cuantas paginas necesitamos mostrar dependiendo de la cantidad de usuarios por limite.
            .skip((page-1)*limit)
            .exec().then((katas:IKata[])=>{
                         
                response.katas=katas;
            });

        //Contar el total de documentos en la coleccion "Kata"
        await kataModel.countDocuments().then((total:number)=>{
            response.totalPages=Math.ceil(total/limit);
            response.currentePage=page;
        });
        return response;

    } catch (error){
            LogError(`[ORM ERROR]: Getting All Katas: ${error}`);
    }
}
// - Get kata by ID
export const getKataByID = async (id:string) : Promise<any | undefined> =>{
    try{
        let kataModel= kataEntity();
// Search kata By ID
    return await kataModel.findById(id);
    } catch (error){
        LogError(`[ORM ERROR]: Getting User by ID: ${error}`);
    }
    
}
// - Delet kata by ID
export const deleteKataByID = async (id:string) : Promise<any | undefined> =>{
    try{
        let kataModel= kataEntity();
        //Borrando una kata por ID
        return await kataModel.deleteOne({_id : id})
    }catch (error){
        LogError(`[ORM ERROR]: Deleting kata by ID: ${error}`);
    }
}

// - Create New kata
export const createKata= async (kata:IKata) : Promise<any | undefined> =>{
    try{
        let kataModel= kataEntity(); 
        //Creado de Usuario en MongoDB 
        return await kataModel.create(kata);
    }catch (error){
        LogError(`[ORM ERROR]: Creating kata: ${error}`);
    }
}

// - Update kata by ID
export const updateKataByID= async (id:string, kata:IKata) : Promise<any | undefined> =>{
    try{
        let kataModel= kataEntity(); 
        //Actaulizar Usuario en MongoDB 
        return await kataModel.findByIdAndUpdate(id,kata);
    }catch (error){
        LogError(`[ORM ERROR]: Creating Kata:${id} ${error}`);
    }

}

