import jwt from 'jsonwebtoken';
import {Request, Response, NextFunction} from 'express';
import dotenv from 'dotenv';

//Configuracion del dotenv para leer el ambiente de las variables
dotenv.config();
const secret= process.env.SECRETKEY||'MYSECRETKEY';

/**
 * 
 * @param {Request} req Solicitud originar previa al middleware para la verificacion del JWT
 * @param {Response} res Respuesta para verificar el JWT
 * @param {NextFunction} next Siguiente funcion a ser ejecutada
 * 
 */

 export const verifyToken = (req: Request, res: Response, next: NextFunction) =>{
    // Verificar el Header desde el Request para 'x-access-token'
    let token: any = req.headers['x-access-token'];
    // Verificar si el JWT esta presnete
    
    if(!token){
        return res.status(403).send({
            authenticationError: 'JWT Missing in request',
            message: 'No esta autorizado a ingresar'
        });
    }
    //Comprobar el token obtenido. Pasamos el secret
    jwt.verify(token,secret,(err:any, decoded:any) =>{
       if(err){
           return res.status(500).send({
               authenticationError: 'JWT Verification Failed',
               message: 'Fallo la verificacion del Token'
           });
        }
        // Si el JWT dio OK -> se ejecutan las funciones Next
        next();
    })
   
}


